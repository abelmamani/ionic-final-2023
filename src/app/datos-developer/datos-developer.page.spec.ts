import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DatosDeveloperPage } from './datos-developer.page';

describe('DatosDeveloperPage', () => {
  let component: DatosDeveloperPage;
  let fixture: ComponentFixture<DatosDeveloperPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(DatosDeveloperPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
