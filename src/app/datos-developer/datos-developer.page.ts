import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@Component({
  selector: 'app-datos-developer',
  templateUrl: './datos-developer.page.html',
  styleUrls: ['./datos-developer.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule]
})
export class DatosDeveloperPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
